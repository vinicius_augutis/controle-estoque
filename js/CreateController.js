(function () {

    angular.module('controleEstoque')

        .config(function ($stateProvider) {
            $stateProvider.state({
                name: 'create',
                url: '/create',
                templateUrl: 'create.html',
                controller: 'CreateController',
                controllerAs: 'vm'
            });

            $stateProvider.state({
                name: 'edit',
                url: '/edit/:id',
                templateUrl: 'create.html',
                controller: 'EditController',
                controllerAs: 'vm'
            });
        })

        .controller('CreateController', function () {

            var vm = this;

            vm.produto = {};

            vm.submeterNovo = function() {

                firebase.database().ref().child('produtos').push(vm.produto);

            }

        })

        .controller('EditController', function ($state , $stateParams) {

            var vm = this;

            vm.produtos = [];

            var ref = firebase.database().ref().child('produtos');

            ref.on('child_added', function(data) {
                var produta = data.val();

                produta.$id = data.key;
                // produto.nome = data.val().nome;

                vm.produtos.push(produta);

            });

            for (var i = 0; i < vm.produtos.length; i++) {
                if(vm.produtos[i].$id == $stateParams.id) {
                    vm.produto = vm.produtos[i] ;
                    break;
                }
            }

            vm.submeterNovo = function() {
                ref.child(vm.produto.$id).update({"nome": vm.produto.nome,
                                                    "qtd": vm.produto.qtd,
                                                        "preco": vm.produto.preco,
                                                            "fornecedor": vm.produto.fornecedor})
                    .then(function() {
                        console.log('Updated');
                    });

            }





        });

})();