(function () {

    angular.module('controleEstoque')

        .config(function ($stateProvider) {
            $stateProvider.state({
                name: 'list',
                url: '/list',
                templateUrl: 'list.html',
                controller: 'ListController',
                controllerAs: 'vm'
            });
        })

        .controller('ListController', ['$scope', function ($scope) {

            var vm = this;

            vm.produtos = [];

            var ref = firebase.database().ref().child('produtos');


            ref.on('child_added', function(data) {
                var produto = data.val();

                produto.$id = data.key;
                // produto.nome = data.val().nome;

                vm.produtos.push(produto);

            });

            vm.removerNode = function (item, index) {
                ref.child(item.$id).remove()
                    .then(function () {
                        console.log("removido com sucesso");
                    });
            }


        }]);

})();